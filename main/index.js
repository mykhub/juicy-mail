const { app, BrowserWindow, globalShortcut } = require('electron');
const path = require('path');
const createUrl = require('url');

let mainWindow;
let dev = false;

if (process.defaultApp
  || /[\\/]electron-prebuilt[\\/]/.test(process.execPath)
  || /[\\/]electron[\\/]/.test(process.execPath)) {
  dev = true;
}

process.env.ELECTRON_DISABLE_SECURITY_WARNINGS = 'true';

const createWindow = () => {
  mainWindow = new BrowserWindow({
    width: 1024,
    height: 768,
    show: false,
    webPreferences: { nodeIntegration: true },
  });

  globalShortcut.register('f5', () => {
    mainWindow.reload();
  });

  globalShortcut.register('CommandOrControl+R', () => {
    mainWindow.reload();
  });

  let indexPath;

  // dev/prod
  if (dev && process.argv.indexOf('--noDevServer') === -1) {
    indexPath = createUrl.format({
      protocol: 'http:',
      host: 'localhost:8080/',
      pathname: 'index.html',
      slashes: true,
    });
  } else {
    indexPath = createUrl.format({
      protocol: 'file:',
      pathname: path.join(__dirname, 'dist', 'index.html'),
      slashes: true,
    });
  }

  mainWindow.loadURL(indexPath)
    .then(name => console.log(`url created: ${name}`))
    .catch(err => console.log(`an error occurred: ${err}`));

  mainWindow.once('ready-to-show', () => {
    mainWindow.show();
    if (dev) {
      const {
        default: installDevtools,
        REDUX_DEVTOOLS,
        REACT_DEVELOPER_TOOLS,
      } = require('electron-devtools-installer'); // eslint-disable-line
      installDevtools([REDUX_DEVTOOLS, REACT_DEVELOPER_TOOLS])
        .then(name => console.log(`Added Extensions: ${name}`))
        .catch(err => console.log(`An error occurred: ${err}`));
      mainWindow.webContents.openDevTools();
    }
  });

  mainWindow.on('closed', () => { mainWindow = null; });
};

app.on('certificate-error', (event, webContents, url, error, certificate, callback) => {
  event.preventDefault();
  callback(true);
});

app.on('ready', createWindow);
app.on('window-all-closed', () => (process.platform !== 'darwin') && app.quit());
app.on('activate', () => {
  if (mainWindow === null) {
    createWindow();
  }
});
