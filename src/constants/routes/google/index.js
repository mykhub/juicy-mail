export * from './mailbox';
export * from './conversation';

export const API_AUTH_GOOGLE = 'api/auth/google';
export const API_AUTH_GOOGLE_STEP2 = 'api/auth/google/step2';
