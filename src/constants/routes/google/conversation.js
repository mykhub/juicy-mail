export const API_CONVERSATION_GOOGLE = 'api/conversation/google';

export const API_GET_CONVERSATION_THREAD_GOOGLE = `${API_CONVERSATION_GOOGLE}/threads/{0}`;

export const API_GET_THREAD_MESSAGES_GOOGLE = `${API_CONVERSATION_GOOGLE}/messages/{0}`;

export const API_GET_INBOX_ZERO_GOOGLE = `${API_CONVERSATION_GOOGLE}/inbox`;
