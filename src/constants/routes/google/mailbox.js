export const API_MAILBOX_GOOGLE = 'api/mailbox/google';

export const API_GET_MAILBOX_BY_ID_GOOGLE = `${API_MAILBOX_GOOGLE}/{0}`;

export const API_GET_MAILBOX_MESSAGES_GOOGLE = `${API_MAILBOX_GOOGLE}/{0}/messages`;

export const API_GET_MAILBOX_CONVERSATIONS_GOOGLE = `${API_MAILBOX_GOOGLE}/{0}/conversations`;

export const API_GET_MAILBOX_ATTACHMENTS_GOOGLE = `${API_MAILBOX_GOOGLE}/{0}/attachments`;

export const API_MAILBOX_DELETE_GOOGLE = `${API_MAILBOX_GOOGLE}/{0}`;
