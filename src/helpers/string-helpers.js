export function formatString(pattern, ...arg) {
  let s = pattern;
  for (let i = 0; i < arg.length; i += 1) {
    const reg = new RegExp(`\\{${i}\\}`, 'gm');
    s = s.replace(reg, arg[i]);
  }
  return s;
}
