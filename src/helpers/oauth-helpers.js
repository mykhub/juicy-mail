const { BrowserWindow } = require('electron').remote;

// TODO: handle platform here.
export class OAuthLoginProxy {
  constructor(platform) {
    this.loginWindow = null;
    this.platform = platform;
  }

  setUrl(url) {
    this.loginWindow.loadURL(url);
  }

  createWindow() {
    const win = new BrowserWindow({
      alwaysOnTop: true,
      webPreferences: { nodeIntegration: true },
    });
    this.loginWindow = win;
  }

  show() {
    this.loginWindow.once('ready-to-show', () => {
      this.loginWindow.show();
    });
  }

  setCloseWindowCallback(callback) {
    this.loginWindow.on('closed', () => {
      callback();
    });
  }

  setOAuthRedirectCallback(callback) {
    this.loginWindow.webContents.on('will-navigate', (event, newUrl) => {
      callback(event, newUrl);
    });
  }

  close() {
    this.loginWindow.close();
  }

  destroy() {
    this.loginWindow = null;
  }
}
