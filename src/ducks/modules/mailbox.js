export const SET_MAILBOXES = 'SET_USER_MAILBOXES';
export const ADD_MAILBOX = 'ADD_MAILBOX';
export const UPDATE_MAILBOX = 'UPDATE_MAILBOX';
export const DELETE_MAILBOX = 'DELETE_MAILBOX';
export const MAILBOX_ERROR = 'MAILBOX_ERROR';

const initialState = {
  byId: [],
  byHash: {},
  errors: [],
};

/* TODO: move to mailbox feature component */

export default function mailbox(state = initialState, action) {
  switch (action.type) {
    case SET_MAILBOXES: {
      const newState = {
        ...state,
        byId: action.mailboxes.map(element => element.id),
        byHash: {},
      };
      action.mailboxes.forEach((element) => {
        newState.byHash[element.id] = element;
      });
      return newState;
    }
    case ADD_MAILBOX: {
      const newState = Object.assign({}, state, {
        byId: [
          ...state.byId,
          action.mailbox.id,
        ],
        byHash: {
          ...state.byHash,
          [action.mailbox.id]: action.mailbox,
        },
      });
      return newState;
    }
    case UPDATE_MAILBOX: {
      const newState = Object.assign({}, state, {
        byId: [...state.byId],
        byHash: {
          ...state.byHash,
          [action.mailbox.id]: action.mailbox,
        },
      });
      return newState;
    }
    case DELETE_MAILBOX: {
      const newState = Object.assign({}, state, {
        byId: [
          ...state.byId,
        ],
        newState: {
          ...state,
        },
      });
      newState.byId.splice(newState.byId.indexOf(action.mailboxId));
      delete newState.byHash[action.mailboxId];
      return newState;
    }
    case MAILBOX_ERROR:
      return {
        byId: [...state.byId],
        byHash: {
          ...state.byHash,
        },
        errors: state.errors.concat([{
          body: action.error.toString(),
          time: new Date(),
        }]),
      };
    default:
      return {
        ...state,
      };
  }
}

export const setMailboxes = mailboxes => ({ type: SET_MAILBOXES, mailboxes });
export const addMailbox = mailboxParam => ({ type: ADD_MAILBOX, mailbox: mailboxParam });
export const updateMailbox = mailboxParam => ({ type: UPDATE_MAILBOX, mailbox: mailboxParam });
export const deleteMailbox = mailboxId => ({ type: DELETE_MAILBOX, mailboxId });
export const mailboxError = error => ({ type: MAILBOX_ERROR, error });
