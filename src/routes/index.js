import React from 'react';
import Login from '../components/auth/login';

export const routes = [
  {
    path: '/login',
    component: Login,
  },
  {
    path: '/',
    component: () => <h1>hello</h1>,
  },
];
