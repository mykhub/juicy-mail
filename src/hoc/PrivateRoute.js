import React from 'react';
import { connect } from 'react-redux';
import { withRouter, Route, Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';

const PrivateRoute = ({ component: Component, pathname, ...rest }) => (
  <Route
    {...rest}
    render={({ location, ...props }) => (
      rest.accessToken
        ? <Component {...props} />
        : <Redirect to={{ pathname /* , state: { from: location } */}} />
    )}
  />
);

PrivateRoute.propTypes = {
  component: PropTypes.func.isRequired,
  pathname: PropTypes.string.isRequired,
};

const mapStateToProps = state => ({
  accessToken: state.auth.accessToken,
});

export default connect(mapStateToProps, null)(withRouter(PrivateRoute));
