import React from 'react';
import { Route, withRouter } from 'react-router';

const SubRoute = route => (
  <Route
    path={route.path}
    render={props => <route.component {...props} routes={route.routes} />}
  />
);

export default withRouter(SubRoute);
