import { put, takeEvery } from 'redux-saga/effects';
import { LOGOUT_CURRENT_USER, currentUserError } from '../components/auth/current-user.module';

function* logoutCurrentUser(user) {
  try {
    localStorage.removeItem(user.emailAddress);
  } catch (ex) {
    yield put(currentUserError, ex);
  }
}

export default function* watchLogoutUser() {
  yield takeEvery(LOGOUT_CURRENT_USER, logoutCurrentUser);
}
