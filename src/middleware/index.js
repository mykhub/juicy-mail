import { all } from 'redux-saga/effects';

import watchAuthGoogle from './auth-google';
import watchSetUser from './set-user';
import watchLogoutUser from './logout-user';

export default function* rootSaga() {
  yield all([
    watchAuthGoogle(),
    watchSetUser(),
    watchLogoutUser(),
  ]);
}
