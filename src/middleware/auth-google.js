import {
  call, take, put, takeEvery,
} from 'redux-saga/effects';
import { eventChannel, END } from 'redux-saga';

import { authorizeGoogleStep2, authorizeGoogle } from '../api/auth';
import {
  GOOGLE_LOGIN_REQUEST,
  loginErrorGoogle,
  loginCancelledGoogle,
  loginSuccessGoogle,
} from '../components/auth/login/login.module';

import { setCurrentUser } from '../components/auth/current-user.module';
import { OAuthLoginProxy } from '../helpers';

function* oAuthStep1(route) {
  try {
    const win = new OAuthLoginProxy();
    win.createWindow();
    win.setUrl(route);
    // Used to connect SAGA with authentication callbacks
    const channel = eventChannel((emitter) => {
      win.setCloseWindowCallback(() => {
        emitter({ closed: true });
        emitter(END);
      });

      // Handles redirect callback
      win.setOAuthRedirectCallback((event, url) => {
        const rawCode = /code=([^&]*)/.exec(url) || null;
        const code = (rawCode && rawCode.length > 1) ? rawCode[1] : null;
        const error = /\?error=(.+)$/.exec(url);

        if (code || error) {
          // Close the browser if code found or error
          win.close();
          win.destroy();
        }

        // If there is a code, proceed to get token from github
        if (code) {
          emitter({ code });
        }
      });
      return () => {
        win.destroy();
      };
    });

    win.show();

    while (true) {
      const { closed, code } = yield take(channel);
      if (code) {
        const response = yield call(authorizeGoogleStep2, code);
        const { data } = response;
        localStorage.setItem(data.emailAddress, JSON.stringify(data));
        yield put(loginSuccessGoogle());
        yield put(setCurrentUser(data));
      }
      if (!closed) {
        break;
      }
      yield put(loginCancelledGoogle());
    }
  } catch (error) {
    yield put(loginErrorGoogle(error));
  }
}

function* loginUsingGoogle() {
  try {
    const res = yield call(authorizeGoogle);
    yield call(oAuthStep1, res.data);
  } catch (error) {
    yield put(loginErrorGoogle(error));
  }
}

function* watchAuthGoogle() {
  yield takeEvery(GOOGLE_LOGIN_REQUEST, loginUsingGoogle);
}

export default watchAuthGoogle;
