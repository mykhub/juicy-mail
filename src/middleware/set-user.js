import {
  put, takeEvery, call,
} from 'redux-saga/effects';
import axios from 'axios';

import { getUserMailboxes } from '../api/mailboxes';
import {
  setMailboxes,
  mailboxError,
} from '../ducks/modules';

import { SET_CURRENT_USER, currentUserError } from '../components/auth/current-user.module';

function* preloadUserMailboxes() {
  try {
    const mailboxes = yield call(getUserMailboxes);
    yield put(setMailboxes, mailboxes);
  } catch (ex) {
    yield put(mailboxError, ex);
  }
}

function* setCurrentUser(data) {
  try {
    axios.defaults.headers.common['Authorization'] = `Bearer ${data.user.accessToken}`;
    yield call(preloadUserMailboxes);
  } catch (ex) {
    yield put(currentUserError, ex);
  }
}

export default function* watchSetUser() {
  yield takeEvery(SET_CURRENT_USER, setCurrentUser);
}
