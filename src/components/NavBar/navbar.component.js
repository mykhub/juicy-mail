import React from 'react';
import {
  NavBar,
  DropDown,
  Button,
  Badge,
  FONTS,
} from 'jc-ui';
import Emojify, { EMOJI_SIZE } from 'react-emojione';

const NavBarContainer = ({ emailAddress, mailboxes }) => (
  <div>
    <NavBar>
      <NavBar.Block>
        <div style={{
          display: 'flex',
          flexDirection: 'row',
          alignItems: 'center',
        }}>
          <span style={{
            fontFamily: FONTS.family.default,
            fontSize: FONTS.fontSize.big,
            lineHeight: '24px',
            fontWeight: 'bold',
          }}>
                         Mailbox name
          </span>
          <div style={{ marginLeft: 'auto' }}>
            <DropDown />
          </div>
        </div>
        <div style={{
          isplay: 'flex',
          flexDirection: 'row',
          alignItems: 'center',
          fontFamily: FONTS.family.default,
          fontSize: FONTS.fontSize.small,
          lineHeight: '24px',
        }}>
          { emailAddress }

        </div>
      </NavBar.Block>
      <NavBar.Block>
        <Button>
          <Emojify style={EMOJI_SIZE(false)}>✏️</Emojify>
              Compose
        </Button>
      </NavBar.Block>
      <NavBar.Block>
        <NavBar.Item>
          <Emojify style={EMOJI_SIZE(true)}>🔎</Emojify>
          <span style={{ marginLeft: '3%' }}>Search</span>
        </NavBar.Item>
        <NavBar.Item>
          <Emojify style={EMOJI_SIZE(true)}>📮</Emojify>
          <span style={{ marginLeft: '3%' }}>Inbox</span>
        </NavBar.Item>
        <NavBar.Item>
          <Emojify style={EMOJI_SIZE(true)}>📮</Emojify>
          <span style={{ marginLeft: '3%' }}>All Threads</span>
        </NavBar.Item>
        <NavBar.Item>
          <Emojify style={EMOJI_SIZE(true)}>📔</Emojify>
          <span style={{ marginLeft: '3%' }}>To-dos</span>
        </NavBar.Item>
      </NavBar.Block>
      <NavBar.Block>
        {mailboxes && mailboxes.byId.length > 0
          ? (
            <DropDown canRotate placeholder="Mailboxes">
              {
                mailboxes.byId.map((id) => {
                  const mailbox = mailboxes.byHash[id];
                  return (
                    <DropDown.Item>
                      <Emojify style={EMOJI_SIZE(true)}>{mailbox.icon}</Emojify>
                      <span style={{ marginLeft: '3%' }}>{mailbox.name}</span>
                    </DropDown.Item>
                  );
                })
              }
            </DropDown>
          )

          : <></>
            }
      </NavBar.Block>
      <NavBar.Block>
        <DropDown canRotate placeholder="Other">
          <DropDown.Item>
            <Emojify style={EMOJI_SIZE(true)}>👪</Emojify>
            <span style={{ marginLeft: '3%' }}>Dream team</span>
          </DropDown.Item>
          <DropDown.Item>
            <Emojify style={EMOJI_SIZE(true)}>👔</Emojify>
            <span style={{ marginLeft: '3%' }}>Work</span>
            <div style={{ marginLeft: 'auto' }}><Badge styles={{ margiLeft: 'auto' }}>+5</Badge></div>
          </DropDown.Item>
          <DropDown.Item>
            <Emojify style={EMOJI_SIZE(true)}>📰</Emojify>
            <span style={{ marginLeft: '3%' }}>Newsletters</span>
            <div style={{ marginLeft: 'auto' }}><Badge styles={{ margiLeft: 'auto' }}>+2</Badge></div>
          </DropDown.Item>
        </DropDown>
      </NavBar.Block>
    </NavBar>
  </div>
);

export default NavBarContainer;
