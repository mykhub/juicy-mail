export const GOOGLE_LOGIN_REQUEST = 'GOOGLE_LOGIN_REQUEST';
export const GOOGLE_LOGIN_SUCCESS = 'GOOGLE_LOGIN_SUCCESS';
export const GOOGLE_LOGIN_ERROR = 'GOOGLE_LOGIN_ERROR';
export const GOOGLE_LOGIN_CANCELLED = 'GOOGLE_LOGIN_CANCELLED';

const initialState = {
  requesting: false,
  successful: false,
  errors: [],
};

export default function authGoogle(state = initialState, action) {
  switch (action.type) {
    case GOOGLE_LOGIN_REQUEST:
      return {
        requesting: true,
        successful: false,
        errors: [],
      };
    case GOOGLE_LOGIN_SUCCESS:
      return {
        requesting: false,
        successful: true,
        errors: [],
      };
    case GOOGLE_LOGIN_ERROR:
      return {
        requesting: false,
        successful: false,
        errors: state.errors.concat([{
          body: action.error.toString(),
          time: new Date(),
        }]),
      };
    case GOOGLE_LOGIN_CANCELLED:
      return {
        requesting: false,
        successful: false,
        errors: [],
      };
    default:
      return {
        ...state,
      };
  }
}

export const loginRequestGoogle = () => ({ type: GOOGLE_LOGIN_REQUEST });
export const loginSuccessGoogle = () => ({ type: GOOGLE_LOGIN_SUCCESS });
export const loginErrorGoogle = error => ({ type: GOOGLE_LOGIN_ERROR, error });
export const loginCancelledGoogle = () => ({ type: GOOGLE_LOGIN_CANCELLED });
