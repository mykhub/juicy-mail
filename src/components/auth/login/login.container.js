import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router';
import PropTypes from 'prop-types';

import Login from './login.component';
import { loginRequestGoogle } from './login.module';

class LoginContainer extends Component {
    loginRequestHandler = () => {
      const { requesting, loginRequestGoogle: loginRequest } = this.props;
      if (!requesting) {
        loginRequest();
      }
    };

    render() {
      const { successful } = this.props;

      return (
        <>
          { successful
            ? <Redirect to="/" />
            : <Login loginRequest={this.loginRequestHandler} />
          }
        </>
      );
    }
}

LoginContainer.propTypes = {
  loginRequestGoogle: PropTypes.func.isRequired,
  requesting: PropTypes.bool.isRequired,
  successful: PropTypes.bool.isRequired,
};

const mapStateToProps = state => ({
  requesting: state.auth.authGoogle.requesting,
  successful: state.auth.authGoogle.successful,
  errors: state.auth.authGoogle.errors,
});

const mapDispatchToProps = {
  loginRequestGoogle,
};

export default connect(mapStateToProps, mapDispatchToProps)(LoginContainer);
