import { combineReducers } from 'redux';

import authGoogle from './login/login.module';
import currentUser from './current-user.module';

const authReducer = combineReducers({
  authGoogle,
  currentUser,
});

export default authReducer;
