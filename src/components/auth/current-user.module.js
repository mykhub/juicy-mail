export const SET_CURRENT_USER = 'SET_CURRENT_USER';
export const LOGOUT_CURRENT_USER = 'LOGOUT_CURRENT_USER';
export const CURRENT_USER_ERROR = 'CURRENT_USER_ERROR';

const initialState = {
  emailAddress: null,
  accessToken: null,
  refreshToken: null,
  errors: [],
};

export default function currentUserModule(state = initialState, action) {
  switch (action.type) {
    case SET_CURRENT_USER:
      return {
        emailAddress: action.user.emailAddress,
        accessToken: action.user.accessToken,
        refreshToken: action.user.refreshToken,
        errors: [],
      };
    case LOGOUT_CURRENT_USER:
      return {
        emailAddress: null,
        accessToken: null,
        refreshToken: null,
        errors: [],
      };
    case CURRENT_USER_ERROR:
      return {
        ...state,
        errors: state.errors.concat([{
          body: action.error.toString(),
          time: new Date(),
        }]),
      };
    default:
      return {
        ...state,
      };
  }
}

export const setCurrentUser = user => ({ type: SET_CURRENT_USER, user });
export const logoutCurrentUser = () => ({ type: LOGOUT_CURRENT_USER });
export const currentUserError = error => ({ type: CURRENT_USER_ERROR, error });
