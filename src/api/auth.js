import axios from 'axios';

import { API_AUTH_GOOGLE, API_AUTH_GOOGLE_STEP2, BASE_URL } from '../constants';

export function authorizeGoogle() {
  return axios.get(`${BASE_URL}${API_AUTH_GOOGLE}`);
}

export function authorizeGoogleStep2(code) {
  return axios.get(`${BASE_URL}${API_AUTH_GOOGLE_STEP2}`, { params: { code } });
}
