import axios from 'axios';

import {
  API_CONVERSATION_GOOGLE,
  API_GET_INBOX_ZERO_GOOGLE,
  API_GET_CONVERSATION_THREAD_GOOGLE,
  API_GET_THREAD_MESSAGES_GOOGLE,
  BASE_URL,
} from '../constants';
import { formatString } from '../helpers';

export function getGoogleConversations() {
  return axios.get(`${BASE_URL}${API_CONVERSATION_GOOGLE}`);
}

export function getInboxZero() {
  return axios.get(`${BASE_URL}${API_GET_INBOX_ZERO_GOOGLE}`);
}

export function getConversationThreads(from) {
  return axios.get(`${BASE_URL}${formatString(API_GET_CONVERSATION_THREAD_GOOGLE, from)}`);
}

export function getThreadMessages(threadId) {
  return axios.get(`${BASE_URL}${formatString(API_GET_THREAD_MESSAGES_GOOGLE, threadId)}`);
}
