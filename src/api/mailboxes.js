import axios from 'axios';

import {
  API_MAILBOX_GOOGLE,
  API_GET_MAILBOX_MESSAGES_GOOGLE,
  API_GET_MAILBOX_ATTACHMENTS_GOOGLE,
  API_GET_MAILBOX_CONVERSATIONS_GOOGLE,
  API_MAILBOX_DELETE_GOOGLE,
  BASE_URL,
} from '../constants';
import { formatString } from '../helpers';

export function getUserMailboxes() {
  return axios.get(`${BASE_URL}${API_MAILBOX_GOOGLE}`);
}

export function getMailboxMessages(mailboxId) {
  return axios.get(`${BASE_URL}${formatString(API_GET_MAILBOX_MESSAGES_GOOGLE, mailboxId)}`);
}

export function getMailboxAttachments(mailboxId) {
  return axios.get(`${BASE_URL}${formatString(API_GET_MAILBOX_ATTACHMENTS_GOOGLE, mailboxId)}`);
}

export function getMailboxConversations(mailboxId) {
  return axios.get(`${BASE_URL}${formatString(API_GET_MAILBOX_CONVERSATIONS_GOOGLE, mailboxId)}`);
}

export function createMailbox(mailbox) {
  return axios.post(`${BASE_URL}${API_MAILBOX_GOOGLE}`, mailbox);
}

export function updateMailbox(mailbox) {
  return axios.put(`${BASE_URL}${API_MAILBOX_GOOGLE}`, mailbox);
}

export function deleteMailbox(mailboxId) {
  return axios.delete(`${BASE_URL}${formatString(API_MAILBOX_DELETE_GOOGLE, mailboxId)}`);
}
