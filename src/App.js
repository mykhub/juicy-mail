import React, { Component } from 'react';
import { connect } from 'react-redux';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import PropTypes from 'prop-types';

import NavBar from './components/NavBar';
import Login from './components/auth/login';
// import { logoutCurrentUser } from './ducks/modules';
import PrivateRoute from './hoc/PrivateRoute';

class App extends Component { // eslint-disable-line
  render() {
    const { emailAddress } = this.props;

    return (
      <Router>
        {emailAddress
          ? <PrivateRoute pathname="/" component={NavBar} />
          : <Route pathname="/login" component={Login} />}
      </Router>
    );
  }
}

App.defaultProps = {
  emailAddress: null,
  // mailboxes: null,
};

App.propTypes = {
  emailAddress: PropTypes.string,
  // mailboxes: PropTypes.instanceOf(Object),
};

const mapStateToProps = state => ({
  emailAddress: state.auth.currentUser.emailAddress,
  mailboxes: state.mailbox,
});
//
// const mapDispatchToProps = {
//   logoutCurrentUser,
// };

export default connect(mapStateToProps, null)(App);
